
class CampaignBase(object):
    """ Base class for Campaigns and Campaign Items """

    base_endpoint = 'backstage/api/1.0/'

    def __init__(self, client, account_id):
        self.client = client
        self.account_id = account_id

    def create_url(self, endpoint=None):
        if not endpoint:
            return self.base_endpoint
        if endpoint.startswith('/'):
            endpoint = endpoint[1:]
        return '{}/{}'.format(self.base_endpoint, endpoint)

    def get(self, element_id):
        return self.send_request('GET', self.create_url(element_id))

    def get_all(self):
        return self.send_request('GET', self.create_url())['results']

    def create(self, **kwargs):
        return self.send_request('POST', self.create_url(), **kwargs)

    def update(self, element_id, **kwargs):
        return self.send_request('POST', self.create_url(element_id), **kwargs)

    def delete(self, element_id):
        return self.send_request('DELETE', self.create_url(element_id))

    def send_request(self, method, path, query_params=None, **kwargs):
        return self.client.send_request(method, path,
                                        query_params=query_params, **kwargs)


class Campaign(CampaignBase):
    """ Campaign class """

    def __init__(self, client, account_id):
        super(Campaign, self).__init__(client, account_id)

    def create_url(self, campaign_id=None):
        campaign_endpoint = '{}/{}/campaigns/'.format(self.base_endpoint, self.account_id)
        if not campaign_id:
            return campaign_endpoint
        if campaign_id.startswith('/'):
            campaign_id = campaign_id[1:]
        return '{}/{}'.format(campaign_endpoint, campaign_id)


class CampaignItem(CampaignBase):
    """ Campaign Items/Children class """

    def __init__(self, client, account_id, campaign_id):
        super(CampaignItem, self).__init__(client, account_id)
        self.campaign_id = campaign_id

    def create_url(self, item_id=None):
        item_endpoint = '{}/{}/campaigns/{}/items/'.format(self.base_endpoint,
                                                           self.account_id,
                                                           self.campaign_id,)
        if not item_id:
            return item_endpoint
        if item_id.startswith('/'):
            item_id = item_id[1:]
        if not item_id.endswith('/'):
            item_id += '/'
        return '{}/{}'.format(item_endpoint, item_id)

    def get_child(self, item_id):
        return self.send_request('GET', self.create_url('children/{}'.format(item_id)))

    def get_all_children(self):
        return self.send_request('GET', self.create_url('children'))

    def create_child(self, **kwargs):
        return self.send_request('POST', self.create_url(), **kwargs)

    def update_child(self, item_id, **kwargs):
        return self.send_request('POST', self.create_url('children/{}'.format(item_id)),
                                 **kwargs)

    # def delete_child(self, item_id):
    #     return self.send_request('DELETE', self.create_url('children/{}'.format(item_id)))


class CampaignError(BaseException):
    """ Handling API errors """
    def __init__(self, error, response):
        self.error = error
        self.response = response

    def __str__(self):
        return 'Code: {}, Error: {}'.format(self.response.status_code, self.error)
