import re

from client.campaign import CampaignError


# Helper function for parsing request response
def parse_response(response):
    if response.ok:
        return response.json()
    else:
        if 'application/json' in response.headers['Content-Type']:
            error = response.json().get('errors', None)
            if not error:
                error = response.json()
                error = error.get('message') if 'offending_field' not in response.json() \
                    else error.get('message') + ': ' + error.get('offending_field')
            else:
                error = response.text
        elif 'application/xml' in response.headers['Content-Type']:
            error = re.search('<error_description>(.*)</error_description>',
                              response.text)
            error = error.group(1) if error else response.text
        else:
            error = response.text

        raise CampaignError(error, response)
