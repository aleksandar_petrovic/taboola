import json
import requests

from helpers import parse_response


class TaboolaClient:
    """ Main client class """

    base_url = 'https://backstage.taboola.com'

    def __init__(self, client_id=None, client_secret=None, access_token=None):
        assert(client_id and client_secret), "You must provide client_id and client_secret"
        self.client_id = client_id
        self.client_secret = client_secret
        self.access_token = access_token

        if not access_token:
            self.__request_token()

    @property
    def token_url(self):
        return 'backstage/oauth/token'.format(self.base_url)

    @property
    def token_details(self):
        return self.send_request('GET', 'backstage/api/1.0/token-details/')

    def __request_token(self):
        result = self.send_request('POST', self.token_url,
                                   token_created=False,
                                   client_id=self.client_id,
                                   client_secret=self.client_secret,
                                   grant_type='client_credentials')

        self.access_token = result.get('access_token')

    # Method for sending requests via request library
    def send_request(self, method, path, query_params=None,
                     token_created=True, **kwargs):
        final_url = '{}/{}'.format(self.base_url, path)
        headers = {}
        if token_created and self.access_token:
            headers = {'Authorization': 'Bearer {}'.format(self.access_token)}
        if method == 'POST' and token_created:
            headers['Content-Type'] = 'application/json'

        data = None
        if token_created and kwargs:
            data = json.dumps(kwargs)
        elif kwargs:
            data = kwargs
        response = requests.request(method, final_url,
                                    data=data,
                                    params=query_params,
                                    headers=headers)
        return parse_response(response)
