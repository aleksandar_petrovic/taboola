from client.taboola_client import TaboolaClient
from client.campaign import Campaign, CampaignItem

# Please provide these two variables with proper id and secret_key
client_id = ''
client_secret = ''

client = TaboolaClient(client_id=client_id, client_secret=client_secret)
account_id = client.token_details['account_id']

# Campaigns
campaign = Campaign(client, account_id)

all_campaigns = campaign.get_all()
one_campaign = campaign.get(all_campaigns[0]['id'])

create_params = {
    "name": "Demo Campaign",
    "branding_text": "Pizza",
    "cpc": 0.01,
    "spending_limit": 1,
    "spending_limit_model": "MONTHLY"
}
new_campaign = campaign.create(**create_params)

update_params = {
    "name": "Demo Campaign - EDITED",
}
updated_campaign = campaign.update(new_campaign['id'], **update_params)

# Campaign Items
campaign_item = CampaignItem(client, account_id, one_campaign['id'])

all_campaign_items = campaign_item.get_all()
one_campaign_item = campaign_item.get(all_campaign_items[0]['id'])

create_params = {
    "url": "http://news.example.com/demo_article.html"
}
new_campaign_item = campaign_item.create(**create_params)

update_params = {
    "title": "UPDATED title",
}
updated_campaign_item = campaign_item.update(new_campaign_item['id'], **update_params)

deleted_item = campaign_item.delete(new_campaign_item['id'])

# RSS Item children
if one_campaign_item['type'] == 'RSS':
    all_children = campaign_item.get_all_children()
    child = campaign_item.get_child(one_campaign_item['id'])

    update_params = {
        "title": "UPDATED title",
    }
    updated_child = campaign_item.update_child(child['id'])
