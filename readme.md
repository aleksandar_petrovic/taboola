# Taboola campaign creation

Python client for campaign creation via Taboola API (https://github.com/taboola/Backstage-API).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine 
for development and testing purposes.

### Requirements

Python 2.7 is needed for this project.

In order to use this client, you need to have API CLIENT_ID and CLIENT_SECRET.
Those API keys are not included with the project, for security reasons.

Things you need to install are in the requirements file. 

```
pip install -r requirements.txt
```

### Testing the client

You can use already provided test.py in the project to test the client.
It already has some examples included.

## Examples

### Connecting/Requesting a token

```python
from client.taboola_client import TaboolaClient
  
client_id = 'ID'
client_secret = 'SECRET'
  
client = TaboolaClient(client_id=client_id, client_secret=client_secret)
   
token_details = client.token_details
```

### Campaigns

You can get all campaigns for the account, get a specific campaign, create and update campaigns.

```python
from client.taboola_client import TaboolaClient
from client.campaign import Campaign
  
client_id = 'ID'
client_secret = 'SECRET'
  
client = TaboolaClient(client_id=client_id, client_secret=client_secret)
  
campaign = Campaign(client, client.token_details['account_id'])
  
# get all campaigns
all_campaigns = campaign.get_all()
  
# get specific campaigns
one_campaign = campaign.get(all_campaigns[0]['id'])
  
# create a campaign (with correct parameters)
create_params = {}
new_campaign = campaign.create(**create_params)
  
# update a campaign (with correct parameters)
update_params = {}
updated_campaign = campaign.update(new_campaign['id'], **update_params)
```

### Campaign Items/Children

Same as with campaigns, you can get all, get specific, create, update and delete items/children.

```python
from client.taboola_client import TaboolaClient
from client.campaign import CampaignItem
  
client_id = 'ID'
client_secret = 'SECRET'
  
client = TaboolaClient(client_id=client_id, client_secret=client_secret)
  
campaign_id = '123'
campaign_item = CampaignItem(client, client.token_details['account_id'], campaign_id)
  
# get all campaign items
all_campaign_items = campaign_item.get_all()
  
# get specific item
one_campaign_item = campaign_item.get(all_campaign_items[0]['id'])
  
# create an item (with correct parameters)
create_params = {}
new_campaign_item = campaign_item.create(**create_params)
  
# update an item (with correct parameters)
update_params = {}
updated_campaign_item = campaign_item.update(new_campaign_item['id'], **update_params)
  
# delete an item (change its status to STOPPED)
deleted_item = campaign_item.delete(new_campaign_item['id'])
  
# If item is of type RSS, it contains children, which can be read/created/updated.
if one_campaign_item['type'] == 'RSS':
    all_children = campaign_item.get_all_children()
    
    child = campaign_item.get_child(one_campaign_item['id'])
  
    update_params = {}
    updated_child = campaign_item.update_child(child['id'])
```

## Built With

* Python 2.7

## Author

* You can contact the author through email: **aleksandar.petrovic.dev@outlook.com**
